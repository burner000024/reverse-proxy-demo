// backend2.js
const express = require('express');
const app = express();

app.get('/', (req, res) => {
    res.send('Hello from the second backend server!');
});

app.listen(3001, () => {
    console.log('Second backend server is running on port 3001');
});
