// proxy.js (extended for load balancing)
const http = require('http');
const httpProxy = require('http-proxy');

const proxy = httpProxy.createProxyServer({});
const targets = ['http://localhost:3000', 'http://localhost:3001'];
let i = 0;

const server = http.createServer((req, res) => {
    const target = targets[i % targets.length];
    i++;
    proxy.web(req, res, { target }, (err) => {
        console.error('Error in proxy:', err);
        res.writeHead(500, { 'Content-Type': 'text/plain' });
        res.end('Something went wrong.');
    });
});

server.listen(8000, () => {
    console.log('Reverse proxy server listening on port 8000');
});
